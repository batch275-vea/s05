<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// to have access with the authenticated user
use Illuminate\Support\Facades\Auth;
// to have access with the queries related to the post entity/model.
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    //action to return a view containing a form for post creation
    public function create(){
        return view('posts.create');
    }

    //action to receive the form data and subsequently store said data in the post table
    public function store(Request $request){
        // if there is an authenticated user
        if(Auth::user()){
            //instantiate a new post object from the Post model
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            //get the id of the authenticated user and set it as foreign key (user_id)
            $post->user_id = (Auth::user()->id);
            //save this post object in the database 
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    //action taht will return a view showing all blog posts
    public function index(){
        /*This is same with */
        //DB::table('Post')->get();
        $posts = Post::get();
        //The "with()" method will alow us to pass information from the controller to view page
        return view('posts.index')->with('posts', $posts);
    }

    //Activity 2
    public function welcome(){
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    //action to show only the posts authored by the authenticated user.
    public function myPost(){
        if(Auth::user()){
            //we are able to fetch the posts related to a specific user because of the establish relationship between the models.
            $posts = Auth::user()->posts;

            return view('posts.mypost')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    //Activity 3 - action that will edit the post
    public function edit($id){
        $post = Post::find($id);
        if(Auth::user()){
            if(Auth::user()->id == $post->user_id){
                return view('posts.edit')->with('post', $post);
            } else {
                return redirect('/posts');
            }
            
        } else {
            return redirect('/login');
        }
    }

    //action to overwrite an existing post when the Put route with matching URL parameter ID is accessed.
    public function update(Request $request, $id){
        $post = Post::find($id);
        // if authenticated user's ID is the same with the post's user_id.
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }

    //action to remove a post with the matching URL ID parameter.
    public function destroy($id){
        $post = Post::find($id);
        //
        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }
        return redirect('/posts');
    }

    //action that will archive post
    public function archive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    //action that will allow authenticated user who is not the post author to toggle a like/unlike on the post being viewed.
    public function likes($id){
        $post = Post::find($id);

        if(Auth::user()){
            $user_id = Auth::user()->id;
            //if the authenticated user is not the post owner.
            if($post->user_id != $user_id){
                //check if a post like has been made
                if($post->likes->contains('user_id', $user_id)) {
                    //delete the like made by the user to unlike this post.
                    PostLike::where('post_id', $post->id)
                        ->where('user_id', $user_id)
                        ->delete();    
                } else {
                    //create a new like record in the post_likes table with the user_id and post_id
                    $postLike = new PostLike;
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;
                    $postLike->save();
                }
            }

                return redirect("posts/$id");//

        } else {
            return redirect ('/login');
        }
    }


    //activity s05
    public function comments(Request $request, $id){
        $post = Post::find($id);
        if(Auth::user()){
            $user_id = Auth::user()->id;
            $postComment = new PostComment;
            $postComment->content = $request->input('content');
            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->save();

            return redirect("posts/$id");
        } else {
            return redirect('/login');
        }
    }

}
