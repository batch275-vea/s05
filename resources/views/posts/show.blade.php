@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			<p class="card-text text-muted">Likes: {{count($post->likes)}} &nbsp;&nbsp; Comments: {{count($post->comments)}}</p>
			
			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">

						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif
					</form>
				@endif

				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
				  Comment
				</button>

				<form  method="POST" action="/posts/{{$post->id}}/comment">
				@csrf
				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
				        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				      </div>
				      <div class="modal-body">
				      	<textarea class="container" type="text" name="content" title="content"></textarea>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Comment</button>
				      </div>
				    </div>
				  </div>
				</div>
				</form>
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>	
	</div>

	<h4 class="mt-5">Comments:</h4>
	@foreach($post->comments as $postComment)
	<div class="card my-2" > 
			<div class="card-body my-2">
				<h4 class="text-center">{{$postComment->content}}</h4>

				<h6 align="right">Posted by: {{$postComment->user->name}}</h6>
                <p align="right" class="card-subtitle text-muted">posted on: {{$postComment->created_at}}</p>
			</div>
	</div>
	@endforeach

@endsection